<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function main()
    {
        return view('register');
    }

    public function signup(Request $request)
    {
        $fname = $request['fname'];
        $lname = $request['lname'];
        $gender = $request['gender'];
        return view('welcome', ['fname' => $fname, 'lname' => $lname, 'gender' => $gender]);
    }
}
