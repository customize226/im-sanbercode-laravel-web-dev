<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Halaman Pendaftaran</title>
</head>

<body>

    <h1>Buat Account Baru!</h1>

    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label for="fname">First name:</label><br><br>
        <input type="text" id="fname" name="fname"><br><br>

        <label for="lname">Last name:</label><br><br>
        <input type="text" id="lname" name="lname"><br><br>

        <label>Gender:</label><br><br>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label><br><br>

        <label for="nationality">Nationality:</label><br><br>
        <select name="nationality">
            <option value="male">Indonesian</option>
            <option value="female">Australian</option>
            <option value="other">Other</option>
        </select><br><br>

        <label for="lspoken">Languange Spoken:</label><br><br>
        <input type="checkbox" id="lspoken1" name="lspoken1" value="bahasa indonesia">
        <label for="lspoken1">Bahasa Indonesia</label><br>
        <input type="checkbox" id="lspoken2" name="lspoken2" value="english">
        <label for="lspoken2">English</label><br>
        <input type="checkbox" id="lspoken3" name="lspoken3" value="other">
        <label for="lspoken3">Other</label><br><br>

        <label for="Bio">Bio:</label><br><br>
        <textarea name="Bio" rows="10" cols="40"></textarea>
        <br>
        <input type="submit" value="signup">
    </form>
</body>

</html>