<?php

class Frog extends Animal
{
    public $jump;

    function __construct($name)
    {
        $this->name = $name;
        $this->legs = 4;
        $this->jump = "Hop Hop";
        $this->cold_blooded = false;
    }

    function jump()
    {
        return $this->jump;
    }
}
