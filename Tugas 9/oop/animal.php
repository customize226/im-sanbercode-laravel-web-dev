<?php

class Animal
{
    public $legs;
    public $cold_blooded;
    public $name;

    public function __construct($name)
    {
        $this->name = $name;
        $this->legs = 4;
        $this->cold_blooded = false;
    }

    public function get_Name()
    {
        return $this->name;
    }

    public function get_Legs()
    {
        return $this->legs;
    }
    public function get_cold_blooded()
    {
        if ($this->cold_blooded == true) {
            return "yes";
        } else {
            return "no";
        }
    }
}
