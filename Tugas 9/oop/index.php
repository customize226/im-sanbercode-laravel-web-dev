<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");
echo "Name : " . $sheep->get_Name() . "<br>";
echo "Legs : " . $sheep->get_Legs() . "<br>";
echo "Cold-blooded : " . $sheep->get_cold_blooded() . "<br><br>";

$kodok = new Frog("buduk");
echo "Name : " . $kodok->get_Name() . "<br>";
echo "Legs : " . $kodok->get_Legs() . "<br>";
echo "Cold-blooded : " . $kodok->get_cold_blooded() . "<br>";
echo "Jump : " . $kodok->jump() . "<br><br>";


$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong->get_Name() . "<br>";
echo "Legs : " . $sungokong->get_Legs() . "<br>";
echo "Cold-blooded : " . $sungokong->get_cold_blooded() . "<br>";
echo "Yell : " . $sungokong->yell() . "<br>";
