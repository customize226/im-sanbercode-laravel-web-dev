<?php

class Ape extends Animal
{
    public $yell;

    function __construct($name)
    {
        $this->name = $name;
        $this->legs = 2;
        $this->yell = "Auooo";
        $this->cold_blooded = false;
    }

    function yell()
    {
        return $this->yell;
    }
}
